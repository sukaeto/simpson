------------------------------------------------------------------------------
--                                                                          --
--                         SIMPLE ADA JSON LIBRARY                          --
--                                                                          --
--                         S I M P S O N . D A T A                          --
--                                                                          --
--                                 B o d y                                  --
--                                                                          --
------------------------------------------------------------------------------
-- Copyright 2011 Jeremy D Keffer <jkeffer@udel.edu>                        --
--                                                                          --
-- Licensed under the Apache License, Version 2.0 (the "License"); you may  --
-- not use this file except in compliance with the License. You may obtain  --
-- a copy of the License at                                                 --
--                                                                          --
--     http://www.apache.org/licenses/LICENSE-2.0                           --
--                                                                          --
-- Unless required by applicable law or agreed to in writing, software      --
-- distributed under the License is distributed on an "AS IS" BASIS,        --
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. --
-- See the License for the specific language governing permissions and      --
-- limitations under the License.                                           --
--                                                                          --
------------------------------------------------------------------------------
-- This file contains the implementation of Ada-equivalents of JSON         --
-- datastructures.                                                          --
--                                                                          --
------------------------------------------------------------------------------

with Ada.Unchecked_Deallocation;
with Ada.Strings.Wide_Wide_Fixed; use Ada.Strings.Wide_Wide_Fixed;

package body SIMPSON.Data is
    use Ada.Strings.Wide_Wide_Unbounded;
    use Operators;
    use type Arrays.Cursor;
    use type Objects.Cursor;

    New_Line : constant Wide_Wide_String := 1 * Wide_Wide_Character'Val(10);

    package body Boxed is
        procedure Free is new
            Ada.Unchecked_Deallocation(Integer, Int_Access);
        procedure Free is new
            Ada.Unchecked_Deallocation(Data.Item, Item_Access);

        function Create(Element : Item_Reference) return Item is
        begin
            return Item'(
                Ada.Finalization.Controlled with
                    Element => Element,
                    Count => new Integer'(1));
        end Create;

        function Get(Self : Item) return Item_Reference is
        begin
            return Self.Element;
        end Get;

        procedure Initialize(Self : in out Item) is
        begin
            Self.Element := new Data.Item'(Kind => A_Null);
            Self.Count := new Integer'(1);
        end Initialize;

        procedure Adjust(Self : in out Item) is
        begin
            Self.Count.all := Self.Count.all + 1;
        end Adjust;

        procedure Finalize(Self : in out Item) is
            Doomed : Item_Access;
        begin
            Self.Count.all := Self.Count.all - 1;
            if Self.Count.all = 0 then
                Doomed := Self.Element;
                Free(Doomed);
                Free(Self.Count);
            end if;
        end Finalize;
    end Boxed;

    function Create(Value : Wide_Wide_String) return Boxed.Item is
    begin
        return Boxed.Create(new Item'(Kind => A_String, Text => +Value));
    end Create;

    function Create(Value : UB.Unbounded_Wide_Wide_String) return Boxed.Item is
    begin
        return Boxed.Create(new Item'(Kind => A_String, Text => Value));
    end Create;

    function Create(Value : Integer) return Boxed.Item is
    begin
        return Boxed.Create(
            new Item'(
                Kind => An_Integer,
                Int => Boxed_Integer'(
                    Value => +Trim(
                        Integer'Wide_Wide_Image(Value),
                        Ada.Strings.Both))));
    end Create;

    function Create(Value : Boxed_Integer) return Boxed.Item is
    begin
        return Boxed.Create(new Item'(Kind => An_Integer, Int => Value));
    end Create;

    function Create(Value : Float) return Boxed.Item is
    begin
        return Boxed.Create(
            new Item'(
                Kind => A_Real,
                Real => Boxed_Real'(
                    Value => +Trim(
                        Float'Wide_Wide_Image(Value),
                        Ada.Strings.Both))));
    end Create;

    function Create(Value : Boxed_Real) return Boxed.Item is
    begin
        return Boxed.Create(new Item'(Kind => A_Real, Real => Value));
    end Create;

    function Create(Value : Boolean) return Boxed.Item is
    begin
        return Boxed.Create(new Item'(Kind => A_Boolean, Bool => Value));
    end Create;

    function Create_Object return Boxed.Item is
        Obj : Item_Reference := new Item(An_Object);
    begin
        return Boxed.Create(Obj);
    end Create_Object;

    function Create_Array return Boxed.Item is
        Arr : Item_Reference := new Item(An_Array);
    begin
        return Boxed.Create(Arr);
    end Create_Array;

    function Create_Null return Boxed.Item is
    begin
        return Boxed.Create(new Item'(Kind => A_Null));
    end Create_Null;

    function Copy(Self : Boxed.Item) return Boxed.Item is
    begin
        case Self.Get.Kind is
            when A_String =>
                return Create(Self.Get.Text);
            when An_Integer =>
                return Create(Self.Get.Int);
            when A_Real =>
                return Create(Self.Get.Real);
            when A_Boolean =>
                return Create(Self.Get.Bool);
            when An_Object =>
            declare
                Obj : Boxed.Item := Create_Object;
                procedure Copy_Element(Position : in Objects.Cursor) is
                begin
                    Objects.Insert(
                        Obj.Get.Object,
                        Objects.Key(Position),
                        Copy(Objects.Element(Position)));
                end Copy_Element;
            begin
                Objects.Iterate(Self.Get.Object, Copy_Element'Access);
                return Obj;
            end;
            when An_Array =>
            declare
                Arr : Boxed.Item := Create_Array;
                procedure Copy_Element(Position : in Arrays.Cursor) is
                begin
                    Arrays.Append(Arr.Get.List, Copy(Arrays.Element(Position)));
                end Copy_Element;
            begin
                Arrays.Iterate(Self.Get.List, Copy_Element'Access);
                return Arr;
            end;
            when A_Null =>
                return Create_Null;
        end case;
    end Copy;

    function Is_Null(Self : Boxed.Item) return Boolean is
    begin
        return Self.Get.Kind = A_Null;
    end Is_Null;

    function Image(Self : Boxed.Item) return Wide_Wide_String is
    begin
        case Self.Get.Kind is
            when A_String =>
                return """" & Escape_String(+Self.Get.Text) & """";
            when An_Integer =>
                return +Self.Get.Int.Value;
            when A_Real =>
                return +Self.Get.Real.Value;
            when A_Boolean =>
                if Self.Get.Bool then
                    return "true";
                else
                    return "false";
                end if;
            when An_Object =>
                return Object_Image(Self);
            when An_Array =>
                return Array_Image(Self);
            when A_Null =>
                return "null";
        end case;
    end Image;

    function Object_Image(Self : Boxed.Item) return Wide_Wide_String is
        Cur : Objects.Cursor := Objects.First(Self.Get.Object);
        Str : Unbounded_Wide_Wide_String;
        Indent : Wide_Wide_String := Tab_Width * Wide_Wide_Character'Val(32);
        From : Natural;
        Before : Natural;
    begin
        Append(Str, '{' & New_Line);

        if Cur /= Objects.No_Element then
            loop
                Append(Str, Indent & """");
                Append(Str, Escape_String(Objects.Key(Cur)));
                Append(Str, """ : ");
                From := Length(Str);

                Append(Str, Image(Objects.Element(Cur)));

                loop
                    Before := Index(Str, New_Line, From);

                    exit when Before = 0;

                    Before := Before + New_Line'Length;
                    From := Before;

                    Insert(Str, Before, Indent);
                end loop;

                Objects.Next(Cur);

                exit when Cur = Objects.No_Element;

                Append(Str, "," & New_Line);
            end loop;
        end if;

        Append(Str, New_Line & '}');
        return To_Wide_Wide_String(Str);
    end Object_Image;

    function Array_Image(Self : Boxed.Item) return Wide_Wide_String is
        Cur : Arrays.Cursor := Arrays.First(Self.Get.List);
        Str : Unbounded_Wide_Wide_String;
        Indent : Wide_Wide_String := Tab_Width * Wide_Wide_Character'Val(32);
        From : Natural;
        Before : Natural;
    begin
        Append(Str, '[' & New_Line);

        if Cur /= Arrays.No_Element then
            loop
                From := Length(Str);
                Append(Str, Image(Arrays.Element(Cur)));

                loop
                    Before := Index(Str, New_Line, From);

                    exit when Before = 0;

                    Before := Before + New_Line'Length;
                    From := Before;

                    Insert(Str, Before, Indent);
                end loop;

                Arrays.Next(Cur);

                exit when Cur = Arrays.No_Element;

                Append(Str, "," & New_Line);
            end loop;
        end if;

        Append(Str, New_Line & ']');
        return To_Wide_Wide_String(Str);
    end Array_Image;

    function Escape_String(Raw : Wide_Wide_String) return Wide_Wide_String is
        Escaped : Unbounded_Wide_Wide_String;

        function Valid_Non_Control_Character(C : Wide_Wide_Character) return Boolean is
        begin
            return
                Wide_Wide_Character'Pos(C) > 31
                and Wide_Wide_Character'Pos(C) /= 127;
        end Valid_Non_Control_Character;
    begin
        for i in Raw'Range loop
            if Raw(i) = '"' then
                Append(Escaped, "\""");
            elsif Raw(i) = '\' then
                Append(Escaped, "\\");
            elsif Wide_Wide_Character'Pos(Raw(i)) =  8 then
                Append(Escaped, "\b");
            elsif Wide_Wide_Character'Pos(Raw(i)) =  12 then
                Append(Escaped, "\f");
            elsif Wide_Wide_Character'Pos(Raw(i)) =  10 then
                Append(Escaped, "\n");
            elsif Wide_Wide_Character'Pos(Raw(i)) =  13 then
                Append(Escaped, "\r");
            elsif Wide_Wide_Character'Pos(Raw(i)) =  9 then
                Append(Escaped, "\t");
            elsif Valid_Non_Control_Character(Raw(i)) then
                Append(Escaped, Raw(i));
            end if;
        end loop;

        return To_Wide_Wide_String(Escaped);
    end Escape_String;
end SIMPSON.Data;
