------------------------------------------------------------------------------
--                                                                          --
--                         SIMPLE ADA JSON LIBRARY                          --
--                                                                          --
--       S I M P S O N . D A T A . N U M B E R _ C O N V E R S I O N        --
--                                                                          --
--                                 B o d y                                  --
--                                                                          --
------------------------------------------------------------------------------
-- Copyright 2011 Jeremy D Keffer <jkeffer@udel.edu>                        --
--                                                                          --
-- Licensed under the Apache License, Version 2.0 (the "License"); you may  --
-- not use this file except in compliance with the License. You may obtain  --
-- a copy of the License at                                                 --
--                                                                          --
--     http://www.apache.org/licenses/LICENSE-2.0                           --
--                                                                          --
-- Unless required by applicable law or agreed to in writing, software      --
-- distributed under the License is distributed on an "AS IS" BASIS,        --
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. --
-- See the License for the specific language governing permissions and      --
-- limitations under the License.                                           --
--                                                                          --
------------------------------------------------------------------------------
-- This file contains the implementation of a series of generic packages to --
-- allow extraction of arbitrary numeric types from JSON structures.        --
--                                                                          --
------------------------------------------------------------------------------

with Ada.Strings.Wide_Wide_Unbounded; use Ada.Strings.Wide_Wide_Unbounded;
with Ada.Strings.Wide_Wide_Fixed; use Ada.Strings.Wide_Wide_Fixed;

package body SIMPSON.Data.Number_Conversion is
    use Operators;
    package body Integer_Conversion is
        function Unbox(Val : Boxed_Integer) return Num is
        begin
            return Num'Wide_Wide_Value(+Val.Value);
        end Unbox;

        function Box(Val : Num) return Boxed_Integer is
        begin
            return Boxed_Integer'(
                Value => +Trim(Num'Wide_Wide_Image(Val), Ada.Strings.Both));
        end Box;
    end Integer_Conversion;

    package body Modular_Conversion is
        function Unbox(Val : Boxed_Integer) return Num is
        begin
            return Num'Wide_Wide_Value(+Val.Value);
        end Unbox;

        function Box(Val : Num) return Boxed_Integer is
        begin
            return Boxed_Integer'(
                Value => +Trim(Num'Wide_Wide_Image(Val), Ada.Strings.Both));
        end Box;
    end Modular_Conversion;

    package body Float_Conversion is
        function Unbox(Val : Boxed_Real) return Num is
        begin
            return Num'Wide_Wide_Value(+Val.Value);
        end Unbox;

        function Box(Val : Num) return Boxed_Real is
        begin
            return Boxed_Real'(
                Value => +Trim(Num'Wide_Wide_Image(Val), Ada.Strings.Both));
        end Box;
    end Float_Conversion;

    package body Fixed_Conversion is
        function Unbox(Val : Boxed_Real) return Num is
        begin
            return Num'Wide_Wide_Value(+Val.Value);
        end Unbox;

        function Box(Val : Num) return Boxed_Real is
        begin
            return Boxed_Real'(
                Value => +Trim(Num'Wide_Wide_Image(Val), Ada.Strings.Both));
        end Box;
    end Fixed_Conversion;

    package body Decimal_Conversion is
        function Unbox(Val : Boxed_Real) return Num is
        begin
            return Num'Wide_Wide_Value(+Val.Value);
        end Unbox;

        function Box(Val : Num) return Boxed_Real is
        begin
            return Boxed_Real'(
                Value => +Trim(Num'Wide_Wide_Image(Val), Ada.Strings.Both));
        end Box;
    end Decimal_Conversion;

    function Create_Boxed_Integer(Token : UB.Unbounded_Wide_Wide_String) return Boxed_Integer is
    begin
        return Boxed_Integer'(Value => Token);
    end Create_Boxed_Integer;

    function Create_Boxed_Real(Token : UB.Unbounded_Wide_Wide_String) return Boxed_Real is
    begin
        return Boxed_Real'(Value => Token);
    end Create_Boxed_Real;
end SIMPSON.Data.Number_Conversion;
