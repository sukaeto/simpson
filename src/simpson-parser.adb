------------------------------------------------------------------------------
--                                                                          --
--                         SIMPLE ADA JSON LIBRARY                          --
--                                                                          --
--                       S I M P S O N . P A R S E R                        --
--                                                                          --
--                                 B o d y                                  --
--                                                                          --
------------------------------------------------------------------------------
-- Copyright 2011 Jeremy D Keffer <jkeffer@udel.edu>                        --
--                                                                          --
-- Licensed under the Apache License, Version 2.0 (the "License"); you may  --
-- not use this file except in compliance with the License. You may obtain  --
-- a copy of the License at                                                 --
--                                                                          --
--     http://www.apache.org/licenses/LICENSE-2.0                           --
--                                                                          --
-- Unless required by applicable law or agreed to in writing, software      --
-- distributed under the License is distributed on an "AS IS" BASIS,        --
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. --
-- See the License for the specific language governing permissions and      --
-- limitations under the License.                                           --
--                                                                          --
------------------------------------------------------------------------------
-- This file contains the implementation of a JSON parser/loader.           --
--                                                                          --
-- The parser is a simple recursive descent parser implementing this        --
-- grammar based on the specification from json.org:                        --
--                                                                          --
--    <value>    ::=   STRING_LITERAL                                       --
--                   | NUMBER_LITERAL                                       --
--                   | <object>                                             --
--                   | <array>                                              --
--                   | TRUE_SYM                                             --
--                   | FALSE_SYM                                            --
--                   | NULL_SYM                                             --
--                                                                          --
--    <object>   ::=   LBRACE_SYM RBRACE_SYM                                --
--                   | LBRACE_SYM <members> RBRACE_SYM                      --
--                                                                          --
--    <members>  ::= <pair> | <pair> COMMA_SYM <members>                    --
--                                                                          --
--    <pair>     ::= STRING_LITERAL COLON_SYM <value>                       --
--                                                                          --
--    <array>    ::=   LBRACKET_SYM RBRACKET_SYM                            --
--                   | LBRACKET_SYM <elements> RBRACKET_SYM                 --
--                                                                          --
--    <elements> ::= <value> | <value> COMMA_SYM <elements>                 --
--                                                                          --
------------------------------------------------------------------------------

with SIMPSON.Lexer;
with Ada.Strings.Wide_Wide_Unbounded; use Ada.Strings.Wide_Wide_Unbounded;
with Ada.Characters.Conversions; use Ada.Characters.Conversions;
with SIMPSON.Data.Number_Conversion;

package body SIMPSON.Parser is
    use Ada.Wide_Wide_Text_IO;
    use type Lexer.Symbol_Type;

    function "+"(Data : Unbounded_Wide_Wide_String) return Wide_Wide_String
        renames To_Wide_Wide_String;

    function Parse(Source : Ada.Wide_Wide_Text_IO.File_Type) return Data.Boxed.Item is
        function Read_All(Source : Ada.Wide_Wide_Text_IO.File_Type) return Wide_Wide_String is
            Current_Line : Unbounded_Wide_Wide_String;
        begin
            if End_Of_File(Source) then
                return "";
            else
                Current_Line := To_Unbounded_Wide_Wide_String(Get_Line(Source));
                return To_Wide_Wide_String(Current_Line) & " " & Read_All(Source);
                --return Get_Line(Source) & " " & Read_All(Source);
            end if;
        end Read_All;
    begin
        if not Is_Open(Source) or else Mode(Source) /= In_File then
            raise Parse_Error with "Source file must be a valid input file";
        end if;

        return Parse(Read_All(Source));
    end Parse;

    function Parse(Source : Wide_Wide_String) return Data.Boxed.Item is
        Text : Lexer.Source_Text := Lexer.New_Source_Text(Source);

        function Value return Data.Boxed.Item;
        function Object return Data.Boxed.Item;
        function Members return Data.Objects.Map;
        function Pair return Data.Objects.Map;
        function Arr return Data.Boxed.Item;
        function Elements return Data.Arrays.Vector;

        function Value return Data.Boxed.Item is
            Token : Lexer.Symbol_Type;
            Token_Text : Unbounded_Wide_Wide_String;
        begin
            Token := Text.Look_Ahead;

            case Token is
                when Lexer.String_Literal =>
                begin
                    Text.Next_Token(Token, Token_Text);
                    return Data.Boxed.Create(
                        new Data.Item'(
                            Kind => Data.A_String, Text => Token_Text));
                exception when Lexer.Lex_Error =>
                    raise Parse_Error with "Invalid String literal " & To_String(To_Wide_Wide_String(Token_Text));
                end;
                when Lexer.Integer_Literal =>
                begin
                    Text.Next_Token(Token, Token_Text);
                    return Data.Boxed.Create(
                        new Data.Item'(
                            Kind => Data.An_Integer,
                            Int => Data.Number_Conversion.Create_Boxed_Integer(
                                Token_Text)));
                exception when Lexer.Lex_Error =>
                    raise Parse_Error with "Invalid Integer literal " & To_String(To_Wide_Wide_String(Token_Text));
                end;
                when Lexer.Real_Literal =>
                begin
                    Text.Next_Token(Token, Token_Text);
                    return Data.Boxed.Create(
                        new Data.Item'(
                            Kind => Data.A_Real,
                            Real => Data.Number_Conversion.Create_Boxed_Real(
                                Token_Text)));
                exception when Lexer.Lex_Error =>
                    raise Parse_Error with "Invalid Real literal " & To_String(To_Wide_Wide_String(Token_Text));
                end;
                when Lexer.LBrace_Sym =>
                begin
                    return Object;
                end;
                when Lexer.LBracket_Sym =>
                begin
                    return Arr;
                end;
                when Lexer.True_Sym | Lexer.False_Sym =>
                begin
                    Text.Next_Token(Token, Token_Text);
                    return Data.Boxed.Create(
                        new Data.Item'(
                            Kind => Data.A_Boolean,
                            Bool => Boolean'Wide_Wide_Value(+Token_Text)));
                exception when Lexer.Lex_Error =>
                    raise Parse_Error with "Udefined Symbol " & To_String(To_Wide_Wide_String(Token_Text)) & " encountered";
                end;
                when Lexer.Null_Sym =>
                declare
                begin
                    Text.Next_Token(Token, Token_Text);
                    return Data.Boxed.Create(
                        new Data.Item'(Kind => Data.A_Null));
                exception when Lexer.Lex_Error =>
                    raise Parse_Error with "Udefined Symbol " & To_String(To_Wide_Wide_String(Token_Text)) & " encountered";
                end;
                when others =>
                begin
                    Text.Next_Token(Token, Token_Text);
                    raise Parse_Error with "Malformed data.  Expected value, got " & To_String(To_Wide_Wide_String(Token_Text));
                end;
            end case;
        end Value;

        function Object return Data.Boxed.Item is
            Object_Item : Data.Boxed.Item := Data.Boxed.Create(
                new Data.Item(Data.An_Object));
            Token : Lexer.Symbol_Type;
            Token_Text : Unbounded_Wide_Wide_String;
        begin
            Text.Next_Token(Token, Token_Text);

            if Token /= Lexer.LBrace_Sym then
                raise Parse_Error with "Expected to see the beginning of an object.";
            end if;

            Token := Text.Look_Ahead;

            if Token = Lexer.RBrace_Sym then
                Text.Next_Token(Token, Token_Text);
                return Object_Item;
            end if;

            Object_Item.Get.Object := Members;

            Text.Next_Token(Token, Token_Text);

            if Token /= Lexer.RBrace_Sym then
                raise Parse_Error with "Expected to see the end of an object.";
            end if;

            return Object_Item;
        end Object;

        function Members return Data.Objects.Map is
            First, Rest : Data.Objects.Map;
            Token : Lexer.Symbol_Type;
            Token_Text : Unbounded_Wide_Wide_String;
        begin
            First := Pair;

            Token := Text.Look_Ahead;

            if Token = Lexer.Comma_Sym then
                Text.Next_Token(Token, Token_Text);
                Rest := Members;
                Rest.Insert(First.First_Key, First.First_Element);
                return Rest;
            end if;

            return First;
        end Members;

        function Pair return Data.Objects.Map is
            One_Pair : Data.Objects.Map;
            Token : Lexer.Symbol_Type;
            Key_Text : Unbounded_Wide_Wide_String;
            Colon_Text : Unbounded_Wide_Wide_String;
        begin
            Text.Next_Token(Token, Key_Text);

            if Token /= Lexer.String_Literal then
                raise Parse_Error with "Invalid String/value pair in object at token " & To_String(To_Wide_Wide_String(Key_Text));
            end if;

            Text.Next_Token(Token, Colon_Text);

            if Token /= Lexer.Colon_Sym then
                raise Parse_Error with "Invalid String/value pair in object at token " & To_String(To_Wide_Wide_String(Colon_Text));
            end if;

            One_Pair.Insert(+Key_Text, Value);
            return One_Pair;
        end Pair;

        function Arr return Data.Boxed.Item is
            Array_Item : Data.Boxed.Item := Data.Boxed.Create(
                new Data.Item(Data.An_Array));
            Token : Lexer.Symbol_Type;
            Token_Text : Unbounded_Wide_Wide_String;
        begin
            Text.Next_Token(Token, Token_Text);

            if Token /= Lexer.LBracket_Sym then
                raise Parse_Error with "Expected to see the beginning of an array.";
            end if;

            Token := Text.Look_Ahead;

            if Token = Lexer.RBracket_Sym then
                Text.Next_Token(Token, Token_Text);
                return Array_Item;
            end if;

            Array_Item.Get.List := Elements;

            Text.Next_Token(Token, Token_Text);

            if Token /= Lexer.RBracket_Sym then
                raise Parse_Error with "Expected to see the end of an array.";
            end if;

            return Array_Item;
        end Arr;

        function Elements return Data.Arrays.Vector is
            Vec : Data.Arrays.Vector;
            Token : Lexer.Symbol_Type;
            Token_Text : Unbounded_Wide_Wide_String;
        begin
            loop
                Data.Arrays.Append(Vec, Value);

                Token := Text.Look_Ahead;

                exit when Token /= Lexer.Comma_Sym;

                Text.Next_Token(Token, Token_Text);
            end loop;

            return Vec;
        end Elements;
    begin
        if Text.Look_Ahead /= Lexer.Eof_Sym then
            return Value;
        else
            return Data.Boxed.Create(new Data.Item'(Kind => Data.A_Null));
        end if;
    end Parse;
end SIMPSON.Parser;
