------------------------------------------------------------------------------
--                                                                          --
--                         SIMPLE ADA JSON LIBRARY                          --
--                                                                          --
--                        S I M P S O N . L E X E R                         --
--                                                                          --
--                                 B o d y                                  --
--                                                                          --
------------------------------------------------------------------------------
-- Copyright 2011 Jeremy D Keffer <jkeffer@udel.edu>                        --
--                                                                          --
-- Licensed under the Apache License, Version 2.0 (the "License"); you may  --
-- not use this file except in compliance with the License. You may obtain  --
-- a copy of the License at                                                 --
--                                                                          --
--     http://www.apache.org/licenses/LICENSE-2.0                           --
--                                                                          --
-- Unless required by applicable law or agreed to in writing, software      --
-- distributed under the License is distributed on an "AS IS" BASIS,        --
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. --
-- See the License for the specific language governing permissions and      --
-- limitations under the License.                                           --
--                                                                          --
------------------------------------------------------------------------------
-- This file contains the implementation of a JSON tokenizer.               --
--                                                                          --
------------------------------------------------------------------------------

with Ada.Unchecked_Deallocation;
with Ada.Characters.Conversions; use Ada.Characters.Conversions;

package body SIMPSON.Lexer is
    procedure Free is new
        Ada.Unchecked_Deallocation(Wide_Wide_String, String_Access);

    function New_Source_Text(Text : Wide_Wide_String) return Source_Text is
    begin
        return
            Source_Text'(Ada.Finalization.Controlled
                with new Wide_Wide_String'(Text), 1);
    end New_Source_Text;

    procedure Next_Token(Source : in out Source_Text; Token : out Symbol_Type; Token_Text : out Unbounded_Wide_Wide_String) is
        function Valid_Number_Character(Char : Wide_Wide_Character) return Boolean is
        begin
            return
                Char in Digit_Character or
                Char = '.' or
                Char = '-' or
                Char = '+' or
                Char = 'e' or
                Char = 'E';
        end Valid_Number_Character;

        function Escaped_Char_Literal(Char : Wide_Wide_Character) return Boolean is
        begin
            return Char = '"' or Char = '\' or Char = '/';
        end Escaped_Char_Literal;

        function Hex_Value(C : Wide_Wide_Character) return Natural is
            Offset : Natural;
        begin
            if C in '0' .. '9' then
                Offset := Wide_Wide_Character'Pos('0');
            elsif C in 'a' .. 'f' then
                Offset := Wide_Wide_Character'Pos('a') - 10;
            elsif C in 'A' .. 'F' then
                Offset := Character'Pos('A') - 10;
            else
                raise Constraint_Error with "Invalid Hex Character";
            end if;

            return Wide_Wide_Character'Pos(C) - Offset;
        end Hex_Value;

        function Code_Point_Value(Code_Point : Wide_Wide_String) return Natural is
            Pos : Natural := 0;
        begin
            for i in Code_Point'Range loop
                Pos := Pos * 16;
                Pos := Pos + Hex_Value(Code_Point(i));
            end loop;

            return Pos;
        end Code_Point_Value;

        procedure Code_Point_Literal(Curr_Char : in out Positive; Token_Text : out Unbounded_Wide_Wide_String) is
            High : Natural;
            Low : Natural;

            function Incomplete_Pair return Boolean is
            begin
                return
                    High in Low_Surrogate or
                    (High in High_Surrogate
                     and Source.Text(Curr_Char + 1 .. Curr_Char + 2) /= "\u");
            end Incomplete_Pair;
        begin
            High := Code_Point_Value(Source.Text(Curr_Char .. Curr_Char + 3));

            Curr_Char := Curr_Char + 3;

            if Incomplete_Pair then
                raise Lex_Error with "Incomplete surrogate pair codepoint \u" & To_String(Source.Text(Curr_Char - 3 .. Curr_Char));
            elsif High in High_Surrogate then
                Curr_Char := Curr_Char + 3;

                Low := Code_Point_Value(Source.Text(Curr_Char .. Curr_Char + 3));

                if Low not in Low_Surrogate then
                    raise Lex_Error with "Invalid end of surrogate pair codepoint \u" & To_String(Source.Text(Curr_Char .. Curr_Char + 3));
                end if;

                Append(
                    Token_Text,
                    Wide_Wide_Character'Val(
                        16#10000# + (High - 16#d800#) * 16#400# + (Low - 16#dc00#)));

                Curr_Char := Curr_Char + 3;
            else
                Append(Token_Text, Wide_Wide_Character'Val(High));
            end if;
        exception when Constraint_Error =>
            raise Lex_Error with "Invalid explicit codepoint \u" & To_String(Source.Text(Curr_Char .. Curr_Char + 3));
        end Code_Point_Literal;

        Curr_Char : Positive := Source.Position;
    begin
        while Curr_Char <= Source.Text'Last and then White_Space(Source.Text(Curr_Char)) loop
            Curr_Char := Curr_Char + 1;
        end loop;

        Source.Position := Curr_Char;

        if Curr_Char > Source.Text'Last then
            Token := Eof_Sym;
            Token_Text := To_Unbounded_Wide_Wide_String("");
        elsif Source.Text(Curr_Char) = '[' then
            Token := LBracket_Sym;
            Token_Text := To_Unbounded_Wide_Wide_String("[");
            Source.Position := Curr_Char + 1;
        elsif Source.Text(Curr_Char) = ']' then
            Token := RBracket_Sym;
            Token_Text := To_Unbounded_Wide_Wide_String("]");
            Source.Position := Curr_Char + 1;
        elsif Source.Text(Curr_Char) = '{' then
            Token := LBrace_Sym;
            Token_Text := To_Unbounded_Wide_Wide_String("{");
            Source.Position := Curr_Char + 1;
        elsif Source.Text(Curr_Char) = '}' then
            Token := RBrace_Sym;
            Token_Text := To_Unbounded_Wide_Wide_String("}");
            Source.Position := Curr_Char + 1;
        elsif Source.Text(Curr_Char) = ':' then
            Token := Colon_Sym;
            Token_Text := To_Unbounded_Wide_Wide_String(":");
            Source.Position := Curr_Char + 1;
        elsif Source.Text(Curr_Char) = ',' then
            Token := Comma_Sym;
            Token_Text := To_Unbounded_Wide_Wide_String(",");
            Source.Position := Curr_Char + 1;
        elsif Source.Text(Curr_Char) = '"' then
            Token := String_Literal;
            Token_Text := Null_Unbounded_Wide_Wide_String;
            Curr_Char := Curr_Char + 1;

            while  Source.Text(Curr_Char) /= '"' loop
                if Source.Text(Curr_Char) = '\' then
                    Curr_Char := Curr_Char + 1;

                    if Escaped_Char_Literal(Source.Text(Curr_Char)) then
                        Append(Token_Text, Source.Text(Curr_Char));
                    elsif Source.Text(Curr_Char) = 'b' then
                        Append(Token_Text, Wide_Wide_Character'Val(8));
                    elsif Source.Text(Curr_Char) = 'f' then
                        Append(Token_Text, Wide_Wide_Character'Val(12));
                    elsif Source.Text(Curr_Char) = 'n' then
                        Append(Token_Text, Wide_Wide_Character'Val(10));
                    elsif Source.Text(Curr_Char) = 'r' then
                        Append(Token_Text, Wide_Wide_Character'Val(13));
                    elsif Source.Text(Curr_Char) = 't' then
                        Append(Token_Text, Wide_Wide_Character'Val(9));
                    elsif Source.Text(Curr_Char) = 'u' then
                        Curr_Char := Curr_Char + 1;
                        Code_Point_Literal(Curr_Char, Token_Text);
                    else
                        raise Lex_Error with "Unfinished escape sequence at Character" & Integer'Image(Curr_Char);
                    end if;

                    Curr_Char := Curr_Char + 1;
                else
                    Append(Token_Text, Source.Text(Curr_Char));
                    Curr_Char := Curr_Char + 1;
                end if;
            end loop;

            Source.Position := Curr_Char + 1;
        elsif Source.Text(Curr_Char) = '-' or Source.Text(Curr_Char) in Digit_Character then
            declare
                Is_Real : Boolean := False;
                Real : Long_Float;
                Int : Long_Integer;
            begin
                while Curr_Char < Source.Text'Last and then Valid_Number_Character(Source.Text(Curr_Char + 1)) loop
                    Curr_Char := Curr_Char + 1;

                    if Source.Text(Curr_Char) = '.' or Source.Text(Curr_Char) = 'e' or Source.Text(Curr_Char) = 'E' then
                        Is_Real := True;
                    end if;
                end loop;

                if Is_Real then
                    Token := Real_Literal;
                    Real := Long_Float'Value(To_String(Source.Text(Source.Position .. Curr_Char)));
                else
                    Int := Long_Integer'Value(To_String(Source.Text(Source.Position .. Curr_Char)));
                    Token := Integer_Literal;
                end if;

                Token_Text := To_Unbounded_Wide_Wide_String(Source.Text(Source.Position .. Curr_Char));
                Source.Position := Curr_Char + 1;
            exception when Constraint_Error =>
                raise Lex_Error with "Malformed Number literal " & To_String(Source.Text(Source.Position .. Curr_Char));
            end;
        elsif Source.Text(Curr_Char) = 't' then
            Curr_Char := Curr_Char + 3;

            if Source.Text(Source.Position .. Curr_Char) = "true" then
                Token := True_Sym;
                Token_Text := To_Unbounded_Wide_Wide_String("true");
            else
                raise Lex_Error with "Malformed true symbol";
            end if;

            Source.Position := Curr_Char + 1;
        elsif Source.Text(Curr_Char) = 'f' then
            Curr_Char := Curr_Char + 4;

            if Source.Text(Source.Position .. Curr_Char) = "false" then
                Token := False_Sym;
                Token_Text := To_Unbounded_Wide_Wide_String("false");
            else
                raise Lex_Error with "Malformed false symbol";
            end if;

            Source.Position := Curr_Char + 1;
        elsif Source.Text(Curr_Char) = 'n' then
            Curr_Char := Curr_Char + 3;

            if Source.Text(Source.Position .. Curr_Char) = "null" then
                Token := Null_Sym;
                Token_Text := To_Unbounded_Wide_Wide_String("null");
            else
                raise Lex_Error with "Malformed null symbol";
            end if;

            Source.Position := Curr_Char + 1;
        else
            raise Lex_Error with "Bad symbol starting with " & To_Character(Source.Text(Curr_Char));
        end if;
    end Next_Token;

    function Look_Ahead(Source : Source_Text; Num_Symbols : Positive := 1) return Symbol_Type is
        Source_Copy : Source_Text := Source;
        Junk : Unbounded_Wide_Wide_String;
        Token : Symbol_Type;
    begin
        for i in 1 .. Num_Symbols loop
            Next_Token(Source_Copy, Token, Junk);
        end loop;

        return Token;
    end Look_Ahead;

    function White_Space(Char : Wide_Wide_Character) return Boolean is
    begin
        return
            Char in Wide_Wide_Character'Val(9) .. Wide_Wide_Character'Val(13) or
            Char = ' ' or
            Char = Wide_Wide_Character'Val(133) or
            Char = Wide_Wide_Character'Val(160);
    end White_Space;

    procedure Initialize(Object : in out Source_Text) is
    begin
        Object.Text := new Wide_Wide_String'("");
    end Initialize;

    procedure Adjust(Object : in out Source_Text) is
    begin
        Object.Text := new Wide_Wide_String'(Object.Text.all);
    end Adjust;

    procedure Finalize(Object : in out Source_Text) is
    begin
        Free(Object.Text);
    end Finalize;
end SIMPSON.Lexer;
