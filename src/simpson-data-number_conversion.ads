------------------------------------------------------------------------------
--                                                                          --
--                         SIMPLE ADA JSON LIBRARY                          --
--                                                                          --
--       S I M P S O N . D A T A . N U M B E R _ C O N V E R S I O N        --
--                                                                          --
--                                 S p e c                                  --
--                                                                          --
------------------------------------------------------------------------------
-- Copyright 2011 Jeremy D Keffer <jkeffer@udel.edu>                        --
--                                                                          --
-- Licensed under the Apache License, Version 2.0 (the "License"); you may  --
-- not use this file except in compliance with the License. You may obtain  --
-- a copy of the License at                                                 --
--                                                                          --
--     http://www.apache.org/licenses/LICENSE-2.0                           --
--                                                                          --
-- Unless required by applicable law or agreed to in writing, software      --
-- distributed under the License is distributed on an "AS IS" BASIS,        --
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. --
-- See the License for the specific language governing permissions and      --
-- limitations under the License.                                           --
--                                                                          --
------------------------------------------------------------------------------
-- This file contains the specification for a series of generic packages to --
-- allow extraction of arbitrary numeric types from JSON structures.        --
--                                                                          --
------------------------------------------------------------------------------

package SIMPSON.Data.Number_Conversion is
    generic
        type Num is range <>;
    package Integer_Conversion is
        function Box(Val : Num) return Boxed_Integer;
        function Unbox(Val : Boxed_Integer) return Num;
        package Operators is
            function "+"(Val : Num) return Boxed_Integer renames Box;
            function "+"(Val : Boxed_Integer) return Num renames Unbox;
        end Operators;
    end Integer_Conversion;

    generic
        type Num is mod <>;
    package Modular_Conversion is
        function Box(Val : Num) return Boxed_Integer;
        function Unbox(Val : Boxed_Integer) return Num;
        package Operators is
            function "+"(Val : Num) return Boxed_Integer renames Box;
            function "+"(Val : Boxed_Integer) return Num renames Unbox;
        end Operators;
    end Modular_Conversion;

    generic
        type Num is digits <>;
    package Float_Conversion is
        function Box(Val : Num) return Boxed_Real;
        function Unbox(Val : Boxed_Real) return Num;
        package Operators is
            function "+"(Val : Num) return Boxed_Real renames Box;
            function "+"(Val : Boxed_Real) return Num renames Unbox;
        end Operators;
    end Float_Conversion;

    generic
        type Num is delta <>;
    package Fixed_Conversion is
        function Box(Val : Num) return Boxed_Real;
        function Unbox(Val : Boxed_Real) return Num;
        package Operators is
            function "+"(Val : Num) return Boxed_Real renames Box;
            function "+"(Val : Boxed_Real) return Num renames Unbox;
        end Operators;
    end Fixed_Conversion;

    generic
        type Num is delta <> digits <>;
    package Decimal_Conversion is
        function Box(Val : Num) return Boxed_Real;
        function Unbox(Val : Boxed_Real) return Num;
        package Operators is
            function "+"(Val : Num) return Boxed_Real renames Box;
            function "+"(Val : Boxed_Real) return Num renames Unbox;
        end Operators;
    end Decimal_Conversion;

    -- These functions are for the benefit of the parser, their use outside of
    -- SIMPSON is not recommended.
    function Create_Boxed_Integer(
        Token : UB.Unbounded_Wide_Wide_String) return Boxed_Integer;
    function Create_Boxed_Real(
        Token : UB.Unbounded_Wide_Wide_String) return Boxed_Real;
end SIMPSON.Data.Number_Conversion;
