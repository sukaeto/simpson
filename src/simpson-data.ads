------------------------------------------------------------------------------
--                                                                          --
--                         SIMPLE ADA JSON LIBRARY                          --
--                                                                          --
--                         S I M P S O N . D A T A                          --
--                                                                          --
--                                 S p e c                                  --
--                                                                          --
------------------------------------------------------------------------------
-- Copyright 2011 Jeremy D Keffer <jkeffer@udel.edu>                        --
--                                                                          --
-- Licensed under the Apache License, Version 2.0 (the "License"); you may  --
-- not use this file except in compliance with the License. You may obtain  --
-- a copy of the License at                                                 --
--                                                                          --
--     http://www.apache.org/licenses/LICENSE-2.0                           --
--                                                                          --
-- Unless required by applicable law or agreed to in writing, software      --
-- distributed under the License is distributed on an "AS IS" BASIS,        --
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. --
-- See the License for the specific language governing permissions and      --
-- limitations under the License.                                           --
--                                                                          --
------------------------------------------------------------------------------
-- This file contains the specification for Ada-equivalents of JSON         --
-- datastructures.                                                          --
--                                                                          --
------------------------------------------------------------------------------

with Ada.Finalization;
with Ada.Strings.Wide_Wide_Unbounded;
with Ada.Containers.Indefinite_Vectors;
with Ada.Containers.Indefinite_Ordered_Maps;

package SIMPSON.Data is
    package UB renames Ada.Strings.Wide_Wide_Unbounded;

    -- Safely use this convenient package without polluting your namespaces.
    package Operators is
        function "+"(
            Data : Wide_Wide_String) return UB.Unbounded_Wide_Wide_String
            renames UB.To_Unbounded_Wide_Wide_String;
        function "+"(
            Data : UB.Unbounded_Wide_Wide_String) return Wide_Wide_String
            renames UB.To_Wide_Wide_String;
    end Operators;

    -- Support for arbitrary size/precision numbers
    type Boxed_Integer is private;
    type Boxed_Real is private;

    -- The width in spaces of one level of indention.  Used in Image functions.
    Tab_Width : constant Natural := 4;

    type Item_Type is (
        A_String,
        An_Integer,
        A_Real,
        A_Boolean,
        An_Object,
        An_Array,
        A_Null
    );

    type Item;
    type Item_Access is access Item;
    subtype Item_Reference is not null Item_Access;

    package Boxed is
        type Item is tagged private;

        function Create(Element : Item_Reference) return Item;

        function Get(Self : Item) return Item_Reference;
    private
        type Int_Access is access Integer;
        type Item is new Ada.Finalization.Controlled with record
            Element : Item_Access;
            Count : Int_Access;
        end record;

        procedure Initialize(Self : in out Item);
        procedure Adjust(Self : in out Item);
        procedure Finalize(Self : in out Item);
    end Boxed;
    use type Boxed.Item;

    package Arrays is new
        Ada.Containers.Indefinite_Vectors(Positive, Boxed.Item);

    package Objects is new
        Ada.Containers.Indefinite_Ordered_Maps(Wide_Wide_String, Boxed.Item);

    -- It's best not to declare one of these directly, but rather use a
    -- Boxed.Item instead.
    type Item(Kind : Item_Type := A_Null) is limited record
        case Kind is
            when A_String =>
                Text : UB.Unbounded_Wide_Wide_String;
            when An_Integer =>
                -- See SIMPSON.Data.Number_Conversion for information on how
                -- to set/extract integer values.
                Int : Boxed_Integer;
            when A_Real =>
                -- See SIMPSON.Data.Number_Conversion for information on how
                -- to set/extract real values.
                Real : Boxed_Real;
            when A_Boolean =>
                Bool : Boolean;
            when An_Object =>
                Object : Objects.Map;
            when An_Array =>
                List : Arrays.Vector;
            when A_Null =>
                null;
        end case;
    end record;

    ------------------------------------------------
    -- Convenience functions for making JSON Data --
    ------------------------------------------------

    function Create(Value : Wide_Wide_String) return Boxed.Item;
    function Create(Value : UB.Unbounded_Wide_Wide_String) return Boxed.Item;

    function Create(Value : Integer) return Boxed.Item;
    function Create(Value : Boxed_Integer) return Boxed.Item;

    function Create(Value : Float) return Boxed.Item;
    function Create(Value : Boxed_Real) return Boxed.Item;

    function Create(Value : Boolean) return Boxed.Item;

    function Create_Object return Boxed.Item;

    function Create_Array return Boxed.Item;

    function Create_Null return Boxed.Item;

    -- Deep copy a JSON structure
    function Copy(Self : Boxed.Item) return Boxed.Item;

    -- Returns true iff Self.Kind is A_Null
    function Is_Null(Self : Boxed.Item) return Boolean;

    -- Returns a valid JSON String of Self.
    function Image(Self : Boxed.Item) return Wide_Wide_String;
private
    type Boxed_Number is record
        Value : UB.Unbounded_Wide_Wide_String;
    end record;

    type Boxed_Integer is new Boxed_Number;
    type Boxed_Real is new Boxed_Number;

    function Object_Image(Self : Boxed.Item) return Wide_Wide_String;
    function Array_Image(Self : Boxed.Item) return Wide_Wide_String;

    -- This will silently throw out all ASCII control characters (save
    -- backspace, formfeed, linefeed, carriage return and horizontal tab since
    -- it escapes those) since these are not valid JSON strings.
    function Escape_String(Raw : Wide_Wide_String) return Wide_Wide_String;
end SIMPSON.Data;
