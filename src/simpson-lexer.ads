------------------------------------------------------------------------------
--                                                                          --
--                         SIMPLE ADA JSON LIBRARY                          --
--                                                                          --
--                        S I M P S O N . L E X E R                         --
--                                                                          --
--                                 S p e c                                  --
--                                                                          --
------------------------------------------------------------------------------
-- Copyright 2011 Jeremy D Keffer <jkeffer@udel.edu>                        --
--                                                                          --
-- Licensed under the Apache License, Version 2.0 (the "License"); you may  --
-- not use this file except in compliance with the License. You may obtain  --
-- a copy of the License at                                                 --
--                                                                          --
--     http://www.apache.org/licenses/LICENSE-2.0                           --
--                                                                          --
-- Unless required by applicable law or agreed to in writing, software      --
-- distributed under the License is distributed on an "AS IS" BASIS,        --
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. --
-- See the License for the specific language governing permissions and      --
-- limitations under the License.                                           --
--                                                                          --
------------------------------------------------------------------------------
-- This file contains the specification for a JSON tokenizer.               --
--                                                                          --
------------------------------------------------------------------------------

with Ada.Finalization;
with Ada.Strings.Wide_Wide_Unbounded; use Ada.Strings.Wide_Wide_Unbounded;

package SIMPSON.Lexer is
    type Source_Text is tagged private;

    type Symbol_Type is (
        LBracket_Sym,
        RBracket_Sym,
        LBrace_Sym,
        RBrace_Sym,
        Colon_Sym,
        Comma_Sym,
        String_Literal,
        Integer_Literal,
        Real_Literal,
        True_Sym,
        False_Sym,
        Null_Sym,
        Eof_Sym
    );

    Lex_Error : exception;

    -- Takes a String representing an JSON data structure and creates a
    -- Source_Text object from it.
    function New_Source_Text(Text : Wide_Wide_String) return Source_Text;

    -- Reads the next token out of Source_Text into Token.  The textual symbol
    -- name of the token is placed into Token_Text
    procedure Next_Token(
        Source : in out Source_Text;
        Token : out Symbol_Type;
        Token_Text : out Unbounded_Wide_Wide_String);

    -- Returns the the token Num_Symbols ahead in the Source_Text.  If there
    -- are less than Num_Symbols remaining, it will return Eof_SYm.
    function Look_Ahead(
        Source : Source_Text;
        Num_Symbols : Positive := 1) return Symbol_Type;
private
    type String_Access is access all Wide_Wide_String;

    type Source_Text is new Ada.Finalization.Controlled with record
        Text : String_Access;
        Position : Positive := 1;
    end record;

    subtype Digit_Character is Wide_Wide_Character range '0' .. '9';

    subtype High_Surrogate is Integer range 16#d800# .. 16#dbff#;
    subtype Low_Surrogate is Integer range 16#dc00# .. 16#dfff#;

    -- Currently defined as Whitespace:
    -- HT, LF, FF, CR, Space, NEL, NBSP
    function White_Space(Char : Wide_Wide_Character) return Boolean;

    procedure Initialize(Object : in out Source_Text);
    procedure Adjust(Object : in out Source_Text);
    procedure Finalize(Object : in out Source_Text);
end SIMPSON.Lexer;
