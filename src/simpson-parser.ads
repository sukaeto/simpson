------------------------------------------------------------------------------
--                                                                          --
--                         SIMPLE ADA JSON LIBRARY                          --
--                                                                          --
--                       S I M P S O N . P A R S E R                        --
--                                                                          --
--                                 S p e c                                  --
--                                                                          --
------------------------------------------------------------------------------
-- Copyright 2011 Jeremy D Keffer <jkeffer@udel.edu>                        --
--                                                                          --
-- Licensed under the Apache License, Version 2.0 (the "License"); you may  --
-- not use this file except in compliance with the License. You may obtain  --
-- a copy of the License at                                                 --
--                                                                          --
--     http://www.apache.org/licenses/LICENSE-2.0                           --
--                                                                          --
-- Unless required by applicable law or agreed to in writing, software      --
-- distributed under the License is distributed on an "AS IS" BASIS,        --
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. --
-- See the License for the specific language governing permissions and      --
-- limitations under the License.                                           --
--                                                                          --
------------------------------------------------------------------------------
-- This file contains the specification for a JSON parser/loader.           --
--                                                                          --
------------------------------------------------------------------------------

with SIMPSON.Data;
with Ada.Wide_Wide_Text_IO;

package SIMPSON.Parser is
    Parse_Error : exception;

    -- Takes a text file containing a JSON data structure and returns an Item
    -- which is an analagous Ada data structure.  Will raise Parse_Error if the
    -- JSON in the file is mal-formed, or if the file is not open or its mode is
    -- not set to In_File.
    function Parse(
        Source : Ada.Wide_Wide_Text_IO.File_Type) return Data.Boxed.Item;

    -- Takes a string representing a JSON data structure and returns an Item
    -- which is an analagous Ada data structure.  Will raise Parse_Error if the
    -- JSON in the string is mal-formed.
    function Parse(Source : Wide_Wide_String) return Data.Boxed.Item;
private
end SIMPSON.Parser;
