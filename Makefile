OPT = -O2 -march=native -mfpmath=sse -msse2
DEBUG = -g -gnato -pg -gnata

simpson: makedirs
	gnatmake $(OPT) -P simpson.gpr
debug: makedirs
	gnatmake $(DEBUG) -P simpson.gpr
makedirs:
	if test -d obj; then true; else mkdir obj; fi
	if test -d lib; then true; else mkdir lib; fi
clean:
	rm obj/*
	rm lib/*
